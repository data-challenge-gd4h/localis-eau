# Challenge GD4H - Localis-eau

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.
 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a> 


## Localis-eau

Dans le cadre d’un projet de recherche adossé à la cohorte Constances, il est prévu d’assigner aux participants de la cohorte les valeurs de divers paramètres de qualité d’eau à leur adresse résidentielle depuis l’an 2000. 

Le tracé vectorisé des unités de distribution de l’eau (UDI) n’étant pas millésimé et remontant au maximum à 2018, il n’est pas possible de savoir par quelles UDI sont desservis les domiciles des participants. Une autre méthode, reposant sur une liaison au point de prélèvement le plus proche, a par conséquent été mise en place.

<a href="https://gd4h.ecologie.gouv.fr/defis/791" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

La méthode est décrite dans ce [document](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/doc/Localiseau_feuilleroute.pdf).
Ensuite, chaque étape est décrite dans le dossier Scripts:
[Load data](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/01_load_data.md),
[Process data](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/02_process_data.md),
[Geocode localization methods](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/03_geocode_localization_methods.md),
[Visualize data](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/04_visualize_data.md),
[Main](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/05_main.md).


### **Installation**

[Guide d'installation](/INSTALL.md)


### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
