# Challenge GD4H - Localis-eau

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.
 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Website</a>

## Localis-eau

As part of a research project supported by the Constances cohort, it is planned to assign the values of various water quality parameters to cohort participants at their residential addresses since the year 2000.
As the vectorized delineation of water distribution units (UDIs) is not dated, and dates back to 2018 at most, it is not possible to know which UDIs serve participants' homes. An alternative method, based on a link to the nearest sampling point, was therefore implemented.

## **Documentation**

The method is described in this [document](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/doc/Localiseau_feuilleroute.pdf).
Then, each step is described in the Scripts folder:
* [Load data](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/01_load_data.md),
* [Process data](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/02_process_data.md),
* [Geocode localization methods](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/03_geocode_localization_methods.md),
* [Visualize data](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/04_visualize_data.md),
* [Main](https://gitlab.com/data-challenge-gd4h/localis-eau/-/blob/main/scripts/05_main.md).

### **Installation**

[Installation Guide](/INSTALL.md)

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
