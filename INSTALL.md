# Installation Guide

## Data Collection

Type files UDI_PLV_2020.txt and UDI_RES_2020.txt must be deposited in the project's "eaurob" folder.
The files can be downloaded here: https://www.data.gouv.fr/fr/datasets/resultats-du-controle-sanitaire-de-leau-du-robinet/#_


## Dependencies

List the dependencies necessary to the project so that it can run locally :
- language, libraries and packages
- how to install them
- recommended : use an environment file such as requirements.txt (py) or DESCRIPTION (R)

## Development

Indicate how to run the solution in development mode locally.

## Production

Indicate, if it exist, a documentation to run the solution in production mode.
