# Main

Ce script R combine plusieurs étapes de traitement, de visualisation et d'analyse de données liées à la géolocalisation. Il importe quatre scripts R externes (de '01_load_data.R' à '04_visualize_methods.R') qui doivent être préalablement définis.

## Chargement des données geocode et polygone des communes

La première section charge et traite les données de géocodage et les données des communes. Les données de géocodage sont traitées avec la fonction `process_geocode_data()`, et les communes sont chargées, traitées et transformées en un tableau de jointure sur le code INSEE.

## Données géographiques des points de prélèvement

Cette section traite les données des points de prélèvement et calcule les distances entre ces points et les centroïdes des communes. Ensuite, les points sont attribués soit au barycentre des points de surveillance, soit au centroïde de la commune. Enfin, la fonction `visualization_wgs()` est utilisée pour visualiser les points de prélèvement.

## Définition des années et de la région

La fonction `define_quality_parameters()` est utilisée pour définir les paramètres de qualité.

## Traitement géographique

Cette section charge et traite les données géographiques des UDI (Unité de Distribution d'eau potable - Inégalités), puis utilise la fonction `traitement_geographique()` pour créer le tableau final. La fonction `final_dataset()` est ensuite utilisée pour créer le tableau1.

## Représentation sur carte

Cette section crée une représentation sur carte des points de prélèvement et des géocodes traités, en utilisant les fonctions `map_representation()` et `visualization_2_methods()`.

## Lecture des données résultats qualité

Cette section charge les données de qualité des UDI avec la fonction `load_all_UDI_RES_data()`.

## Jointures

Cette section effectue une série de jointures pour créer le tableau final.

## Graphiques et corrélation

La dernière section du script crée une série de graphiques pour chaque région et année, à la fois avec et sans valeurs aberrantes. Enfin, elle calcule les coefficients de corrélation pour chaque paramètre, région et type de commune.