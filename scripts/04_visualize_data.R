source("03_geocode_localization_methods.R")

##### 0) chargement packages  ####################################################################
install.packages(c("ggpubr", "dplyr"))

library(ggpubr)
library(dplyr)

######## 1) Représentation des points de prélèvements wgs ##################################
visualization_wgs <- function(localisation_point_prelev_wgs) {
  leaflet() %>%
    addTiles() %>%
    addGlPoints(data = localisation_point_prelev_wgs, popup = ~cdpointsurv)
}

######## 2) Représentation sur carte des 2 méthodes ##################################
map_representation <- function(localisation_point_prelev_wgs, tableau1, geocode_wgs, all_shape_udi) {
  points_retenus_wgs <- localisation_point_prelev_wgs %>%
    left_join(tableau1) %>%
    filter(!is.na(ident))

  geocode_traite <- geocode_wgs %>%
    left_join(tableau1) %>%
    filter(!is.na(code_udi))

  return(list(points_retenus_wgs = points_retenus_wgs, geocode_traite = geocode_traite))
}

visualization_2_methods <- function(all_shape_udi_wgs, points_retenus_wgs, geocode_traite) {
  leaflet() %>%
    addTiles() %>%
    addPolygons(data = all_shape_udi_wgs) %>%
    addGlPoints(points_retenus_wgs, fillColor = "red", popup = ~cdpointsurv) %>%
    addGlPoints(geocode_traite, fillColor = "green", popup = ~ paste(
      "<b>", "Geocode", ident, "</b>", "<br>",
      "code_udi:", code_udi, "<br>",
      "point de prelev le plus proche:", cdpointsurv, "<br>",
      "Distance:", distance, "<br>"
    ))
}

######## 3) Comparaison des 2 méthodes ##################################
graphe_outlier <- function(data, parametre, region_select, annee_select, retirer_outlier) {
  options(repr.plot.width = 20, repr.plot.height = 10)

  data <- data %>%
    filter(!is.na(mediane_udi), !is.na(mediane_point)) %>%
    filter(cdparametresiseeaux %in% parametre, region == region_select, annee == annee_select)

  if (retirer_outlier) {
    data <- data %>%
      group_by(cdparametresiseeaux) %>%
      mutate(limite_outlier_udi = (quantile(mediane_udi, 0.75, na.rm = TRUE) - quantile(mediane_udi, 0.25, na.rm = TRUE)) * 3) %>%
      mutate(limite_outlier_point = (quantile(mediane_point, 0.75, na.rm = TRUE) - quantile(mediane_point, 0.25, na.rm = TRUE)) * 3) %>%
      ungroup() %>%
      filter(mediane_udi <= limite_outlier_udi, mediane_point <= limite_outlier_point)
  }

  data %>%
    group_by(cdparametresiseeaux) %>%
    mutate(cdparametresiseeaux = paste0(cdparametresiseeaux, " (n = ", n(), ")")) %>%
    ungroup() %>%
    ggplot() +
    geom_point(aes(x = mediane_udi, y = mediane_point, color = categorie_zone)) +
    geom_smooth(aes(x = mediane_udi, y = mediane_point), method = "lm", se = FALSE, color = "blue", formula = y ~ x) +
    stat_cor(aes(x = mediane_udi, y = mediane_point), method = "spearman") +
    facet_wrap(~cdparametresiseeaux, scales = "free") +
    scale_color_manual(values = c("Rural" = "navy", "Urban_moyen" = "green", "Urban_fort" = "gold")) + # Couleurs personnalisées
    theme_bw() +
    ggtitle(label = paste(region_select, annee_select))
}
