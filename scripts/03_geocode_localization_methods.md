# Geocode localization methods

Ce script R réalise un traitement géographique pour une sélection spécifique de région, d'année et de distance maximale. Il identifie les géocodes qui sont soit situés dans une Unité de Distribution Informatique (UDI) spécifique, soit proches d'un point de prélèvement. Le script produit ensuite un tableau final contenant ces informations.

Le script est principalement composé de deux fonctions :

## traitement_geographique()

Cette fonction prend en paramètres :

    `region_select` : La région sélectionnée pour le traitement.
    `annee_select` : L'année sélectionnée pour le traitement.
    `distance_max` : La distance maximale du géocode par rapport à un point de prélèvement.
    `geocode` : Les géocodes à traiter.
    `localisation_point_prelev` : Les points de prélèvement.

La fonction utilise deux méthodes :

    Méthode 1 : Identification des géocodes situés dans l'UDI spécifique.
    Méthode 2 : Identification du point de prélèvement le plus proche pour chaque géocode.

Le résultat de cette fonction est un tableau contenant le code UDI et les coordonnées du point de prélèvement le plus proche (parmi ceux ayant une fiabilité de coordonnées non nulle), la distance à ce point, ainsi que la fiabilité des coordonnées pour ce point de prélèvement.

## final_dataset()

Cette fonction prend en paramètres :

    `regions` : Les régions à traiter.
    `annees` : Les années à traiter.
    `distance_max` : La distance maximale du géocode par rapport à un point de prélèvement.

Elle appelle la fonction `traitement_geographique()` pour chaque combinaison de région et d'année, et produit un tableau final contenant l'identifiant du géocode, le code UDI, le code du point de prélèvement, la distance au point de prélèvement, la fiabilité des coordonnées, l'année et la région.

Remarque : Ce script nécessite l'installation et l'utilisation des packages `tidyverse` et `sf` pour le traitement des données et la gestion des géocodes.