# Process data

Ce script R est divisé en trois parties principales : le chargement des données géocodées, le traitement des données géographiques des points de prélèvement, et la préparation pour l'analyse des corrélations.

## Chargement des packages nécessaires

Le script commence par installer et charger le package 'leafgl' qui fournit des outils pour manipuler et visualiser des données géographiques.

## Chargement des données géocodées

Cette partie du script comprend deux fonctions principales `process_geocode_data` et `process_communes` qui transforment les tables de données en utilisant une projection de coordonnées spécifique (EPSG:4326 pour les coordonnées en WGS84) et calculent les coordonnées des centroïdes des communes respectivement.

## Données géographiques des points de prélèvement

Cette partie comprend quatre fonctions principales : `point_prelev_wgs84`, `point_prelev`, `distance_point_prelev`, et `data_point_prelev`. Ces fonctions traitent les points de prélèvement en WGS84, rassemblent les points Lambert 93 et WGS 84 retraités en Lambert 93, retirent les valeurs aberrantes de Lambert 93, et comparent les distances entre les points de prélèvement et les centroïdes de la commune respectivement.

## Préparation pour l'analyse des corrélations

Cette partie comprend trois fonctions principales : `joints_median`, `create_final_dataset`, et `correlation_calculated`. Ces fonctions préparent le dataset pour l'analyse des corrélations en effectuant une jointure sur les données de prélèvement et les résultats, en créant le dataset final avec des valeurs médianes par point de surveillance et UDI, et en calculant les coefficients de corrélation pour chaque paramètre, région et type de commune respectivement.

## Conclusion

En somme, ce script R fournit des fonctions utiles pour traiter les données géographiques, effectuer des jointures et des agrégations sur les données de surveillance de l'eau, et préparer ces données pour une analyse de corrélation ultérieure. Cela pourrait être utilisé par les chefs de projet ou les développeurs qui travaillent sur des projets liés à l'analyse de données environnementales, en particulier celles liées à la surveillance de la qualité de l'eau.