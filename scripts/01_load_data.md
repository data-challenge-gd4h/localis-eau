# Load data

Ce script R est utilisé pour charger, manipuler et analyser les données de qualité de l'eau à partir de plusieurs fichiers et sources. Il est utilisé pour lire les données géographiques, les données de prélèvements d'eau, définir des paramètres de qualité et des régions, lire l'ensemble des données 'shape' UDI, et lire les données de qualité de l'eau.

## Description des fonctions
### Installation et chargement des packages

Cette section installe et charge plusieurs packages nécessaires pour le script. Les packages comprennent `data.table`, `tidyverse`, `sf`, `leaflet`, `leaflet.extras` et `janitor`.

### Chargement des données géocodées

La fonction `load_geocode_data()` est utilisée pour charger les données géocodées à partir d'un fichier CSV spécifié par `geocode_path`. Les coordonnées X et Y sont ensuite converties en une tibble, avec Y converti en numérique. Les coordonnées sont ensuite converties en un simple feature collection.

La fonction `load_commune_data()` est utilisée pour charger les données de la commune à partir d'un fichier SHP spécifié par `commune_path`. La surface de la commune est calculée et ajoutée à la collection de caractéristiques simples.

### Chargement des données géographiques des points de prélèvements

La fonction `load_all_UDI_PLV_data()` est utilisée pour charger les données géographiques de tous les points de prélèvements d'eau. Elle recherche tous les fichiers correspondant au motif `UDI_PLV` dans le dossier spécifié par data_folder et combine les données de tous ces fichiers en un seul dataframe.

### Définition des années et des régions

La fonction `define_quality_parameters()` définit un certain nombre de régions, d'années et de paramètres de qualité pour l'analyse.

### Lecture de l'ensemble des shapes UDI

La fonction `lecture_shape_udi()` lit les données 'shape' UDI pour une région spécifiée. Elle change le nom de la colonne appropriée pour correspondre à 'code_udi'.

La fonction `lecture_all_shape_udi()` utilise la fonction précédente pour lire les données 'shape' UDI pour toutes les régions spécifiées et les combine en un seul dataframe.

### Lecture des données qualité

La fonction `load_all_UDI_RES_data()` est utilisée pour charger les données de qualité de l'eau. Elle recherche tous les fichiers correspondant au motif `UDI_RES` dans le dossier spécifié par `data_folder` et combine les données de tous ces fichiers en un seul dataframe.

## Utilisation

Pour utiliser ce script, les fonctions doivent être appelées avec les paramètres appropriés. Par exemple, pour charger les données géocodées, la fonction `load_geocode_data()` doit être appelée avec le chemin vers le fichier CSV contenant les données géocodées. De même, pour charger les données de prélèvement d'eau, la fonction `load_all_UDI_PLV_data()` doit être appelée avec le chemin vers le dossier contenant les fichiers de données.

## Conclusion

Ce script est un outil complet pour l'analyse des données de qualité de l'eau. Il fournit des fonctions pour charger et préparer les données, définir des paramètres d'analyse, et lire les données de qualité. Pour une utilisation efficace, il est recommandé d'avoir une compréhension de base de R et des packages utilisés dans le script.
