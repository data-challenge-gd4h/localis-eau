# Visualize data

Ce script R effectue une visualisation et une comparaison des méthodes de géolocalisation utilisées dans le script précédent (`02_process_data.R`). Le script est principalement composé de quatre fonctions :

## visualization_wgs()

Cette fonction prend en entrée `localisation_point_prelev_wgs` qui représente les points de prélèvement dans le système de coordonnées WGS. Elle crée une carte interactive avec `leaflet()`, sur laquelle elle affiche les points de prélèvement.

## map_representation() et visualization_2_methods()

La fonction `map_representation()` prend en entrée `localisation_point_prelev_wgs`, `tableau1`, `geocode_wgs` et `all_shape_udi`. Elle fusionne ces données et filtre les géocodes traités.

La fonction `visualization_2_methods()` prend en entrée `all_shape_udi_wgs`, `points_retenus_wgs` et `geocode_traite`. Elle crée une carte interactive avec `leaflet()`, sur laquelle elle affiche les UDI en tant que polygones, les points de prélèvement sélectionnés en rouge et les géocodes traités en vert.

## graphe_outlier()

Cette fonction sert à visualiser la comparaison des deux méthodes. Elle prend en entrée `data` qui est le tableau de données, `parametre` qui est le paramètre sélectionné, `region_select` qui est la région sélectionnée, `annee_select` qui est l'année sélectionnée et `retirer_outlier` qui est un booléen indiquant si les valeurs aberrantes doivent être retirées ou non.

La fonction effectue un certain nombre de transformations sur les données, y compris le retrait des valeurs aberrantes, si spécifié. Elle crée ensuite un graphique à facettes, où chaque facette représente un paramètre, et chaque point représente une observation. Une ligne de régression est tracée pour chaque facette, et un coefficient de corrélation de Spearman est calculé pour chaque facette.

Remarque : Ce script nécessite l'installation et l'utilisation des packages `ggpubr`, `dplyr` et `leaflet` pour le traitement des données et la visualisation.