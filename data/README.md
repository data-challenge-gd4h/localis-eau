Les données brutes peuvent pour la plupart se trouver en opendata:

* Pour les données brutes de l'INSEE : https://www.data.gouv.fr/fr/datasets/resultats-du-controle-sanitaire-de-leau-du-robinet/

* Pour les données SISE eaux : https://data.eaufrance.fr/

Seul le fichier contenant les geocodes est aléatoire dans ce projet.
