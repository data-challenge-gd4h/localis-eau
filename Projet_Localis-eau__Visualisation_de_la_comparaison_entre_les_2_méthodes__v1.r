# Importation des librairies
library(ggpubr)
library(data.table)
library(tidyverse)
library(dplyr)

# Importation du fichier tableau_pour_analyse.csv (fichier avec les différentes jointures) 
tableau_analyse <- fread("tableau_pour_analyse.csv", sep=";") 

# aperçu du fichier
head(tableau_analyse)


# Ajouter une nouvelle colonne avec les catégories spécifiées
tableau_analyse <- tableau_analyse %>%
  mutate(categorie_zone = case_when(
    TUU2017_rural == 99 ~ "Rural",
    TUU2017_rural %in% c(1, 2, 3, 4, 5, 6) ~ "Urban_moyen",
    TUU2017_rural %in% c(7, 8) ~ "Urban_fort",
    TRUE ~ NA_character_
  ))

# Afficher le nouveau tableau avec la colonne ajoutée
head(tableau_analyse)


# Conversion des colonnes en valeurs numériques
tableau_analyse <- tableau_analyse %>%
  mutate(mediane_udi = as.numeric(gsub(",", ".", mediane_udi)),
         mediane_point = as.numeric(gsub(",", ".", mediane_point)))

############ rappel du contexte #################

# paramètres qualité à prendre en compte 
liste_parametres_qualite <- c("BR",  "BRF", "BRURE" , "CDT", "CDT25", "CL2LIB", "CL2TOT" , "CLATE",
                              "CLF",
                              "CLITE", "CLITEMG", "COD" , "COT", "DBRMCL" ,
                              "DCLMBR", "MOAC", "MOAF", "MOBC", "NO3", "PH", "TAC", "THM", "THM4")

# Définition des Annnees et région 

regions <- c("Grand_Est", "Nouvelle_Aquitaine", "Bretagne", "Centre_Val_de_Loire", "Auvergne_Rhone_Alpes",
             "Bourgogne_Franche_Comte" , "Normandie", "Occitanie", "Pays_de_la_Loire", "PACA")
annees <- c(2021, 2020, 2023, 2021, 2021, 
            2019, 2021,2021,2018,2022 ) 

# ############ visualisation #################

# Comparaison des 2 méthodes
# Région Grand_Est avec outliers
options(repr.plot.width=20, repr.plot.height=10)

graphe_1 <- function(data = tableau_analyse, parametre = liste_parametres_qualite, 
                     region_select = "Grand_Est", annee_select = 2021,
                     retirer_outlier = FALSE) {
  
  data <- data %>%
    filter(!is.na(mediane_udi), !is.na(mediane_point)) %>%
    filter(cdparametresiseeaux %in% parametre, region == region_select, annee == annee_select)
  
  if (retirer_outlier) {
    data <- data %>%
      group_by(cdparametresiseeaux) %>%
      mutate(limite_outlier_udi = (quantile(mediane_udi, 0.75, na.rm = TRUE) - quantile(mediane_udi, 0.25, na.rm = TRUE)) * 3) %>%
      mutate(limite_outlier_point = (quantile(mediane_point, 0.75, na.rm = TRUE) - quantile(mediane_point, 0.25, na.rm = TRUE)) * 3) %>%
      ungroup() %>%
      filter(mediane_udi <= limite_outlier_udi, mediane_point <= limite_outlier_point)
  }
  
  data %>%
    group_by(cdparametresiseeaux) %>%
    mutate(cdparametresiseeaux = paste0(cdparametresiseeaux, " (n = ", n(), ")")) %>%
    ungroup() %>%
    ggplot() +
    geom_point(aes(x = mediane_udi, y = mediane_point, color = categorie_zone)) +
    geom_smooth(aes(x = mediane_udi, y = mediane_point), method = "lm", se = FALSE, color = "blue", formula = y ~ x) +
    stat_cor(aes(x = mediane_udi, y = mediane_point), method = "spearman") +
    facet_wrap(~cdparametresiseeaux, scales = "free") +
    scale_color_manual(values = c("Rural" = "navy", "Urban_moyen" = "green", "Urban_fort" = "gold")) +  # Couleurs personnalisées
    theme_bw() +
    ggtitle(label = paste(region_select, annee_select))
}

graphe_1()

# Région Grand_Est sans outliers

print("Region Grand_Est_2021 sans outliers")
graphe_2 <- function(data = tableau_analyse, parametre =liste_parametres_qualite, 
                       region_select = "Grand_Est",annee_select =2021,
                       retirer_outlier = TRUE){
    
   data <-  data %>% 
      filter(!is.na(mediane_udi) , !is.na(mediane_point)) %>% 
      filter(cdparametresiseeaux %in% parametre, region ==region_select, annee ==annee_select) 
  
    
   if(retirer_outlier ){
     
    data <-  data %>% 
       group_by( cdparametresiseeaux) %>% #annee, region,
       mutate(limite_outlier_udi = (quantile(mediane_udi,0.75, na.rm=T)- quantile(mediane_udi,0.25,na.rm=T))*3 ) %>% 
       mutate(limite_outlier_point = (quantile(mediane_point,0.75, na.rm=T)- quantile(mediane_point,0.25, na.rm=T))*3 ) %>% 
      ungroup() %>% 
       filter(mediane_udi <= limite_outlier_udi, mediane_point <= limite_outlier_point) 
   }
    
data %>% 
  group_by(cdparametresiseeaux) %>%
  mutate(cdparametresiseeaux = paste0(cdparametresiseeaux, " (n = ", n(), ")")) %>%
  ungroup() %>%
      ggplot()+ geom_point(aes(x= mediane_udi, y = mediane_point,color = categorie_zone))+
      geom_smooth(aes(x=  mediane_udi, y = mediane_point),method = "lm", se=FALSE, color="blue", formula =  y ~ x)+
      stat_cor(aes(x=  mediane_udi, y = mediane_point),method = "spearman")+
      facet_wrap(~ cdparametresiseeaux, scales = "free")+
      scale_color_manual(values = c("Rural" = "navy", "Urban_moyen" = "green", "Urban_fort" = "gold")) +  # Couleurs personnalisées
      theme_bw() +
      ggtitle(label = paste(region_select,annee_select ))
  }
 
  graphe_2() 

# Toutes les régions
all_graphes_avec_outliers <-  map2(regions, annees, ~ graphe_1(region_select= .x, annee_select  = .y))

print("Region Nouvelle_Aquitaine")
all_graphes_avec_outliers[[2]]


all_graphes_sans_outliers <-  map2(regions, annees, ~ graphe_2(region_select= .x, annee_select  = .y))

print("Region Nouvelle_Aquitaine sans outliers")
all_graphes_sans_outliers[[2]]

print("Region Bretagne")
all_graphes_avec_outliers[[3]]

print("Region Bretagne sans outliers")
all_graphes_sans_outliers[[2]]

print("Region Centre_Val_de_Loire")
all_graphes_avec_outliers[[4]]

print("Region Centre_Val_de_Loire sans outliers")
all_graphes_sans_outliers[[4]]

print("Region Auvergne_Rhone_Alpes")
all_graphes_avec_outliers[[5]]

print("Region Auvergne_Rhone_Alpes sans outliers")
all_graphes_sans_outliers[[5]]

print("Region Bourgogne_Franche_Comte")
all_graphes_avec_outliers[[6]]

print("Region Bourgogne_Franche_Comte sans outliers")
all_graphes_sans_outliers[[6]]

print("Region Normandie")
all_graphes_avec_outliers[[7]]

print("Region Normandie sans outliers")
all_graphes_sans_outliers[[7]]

print("Region Occitanie")
all_graphes_avec_outliers[[8]]

print("Region Occitanie sans outliers")
all_graphes_sans_outliers[[8]]

print("Region Pays_de_la_Loire")
all_graphes_avec_outliers[[9]]

print("Region Pays_de_la_Loire sans outliers")
all_graphes_sans_outliers[[9]]

print("Region PACA")
all_graphes_avec_outliers[[10]]

print("Region PACA sans outliers")
all_graphes_sans_outliers[[10]]


# Calculer les coefficients de corrélation pour chaque paramètre, région et type_com
cor_summary <- tableau_analyse %>%
  filter(!is.na(mediane_udi), !is.na(mediane_point)) %>%
  filter(cdparametresiseeaux %in% liste_parametres_qualite) %>%
  group_by(cdparametresiseeaux, region) %>%
  summarise(correlation = cor(mediane_udi, mediane_point, method = "spearman"), .groups = "drop")

# Réorganiser le tableau 
cor_summary_pivot <- cor_summary %>%
  pivot_wider(names_from = cdparametresiseeaux, values_from = correlation)

# Afficher le tableau  réorganisé
cor_summary_pivot






